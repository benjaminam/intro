<?php
    class Message {
        private $text = "A simple message";
        public static $count = 0;
        public function show(){
            echo "<p>$this->text</p>";
        }
        function __construct($text = ""){
            ++self::$count;
            if($text !=""){
                $this->text = $text;
            }
        }
    }
?>