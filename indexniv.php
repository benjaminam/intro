<?php
    include "class.php";
?>
<html>
    <head>
        <title>Object Oriented PHP</title>
    </head>
    <body>
        <p>
        <?php
            $text = 'Hello World';
            echo "$text And The Universe";
            echo '<br>';
            $msg = new Message();
            echo '<br>';
            echo Message::$count;
            //echo $msg->text;
            $msg->show();
            $msg1 = new Message("A new text");
            $msg1->show();
            echo '<br>';
            echo Message::$count;
            $msg2 = new Message();
            $msg2->show();
            echo '<br>';
            echo Message::$count;
        ?>
        </P>
    </body>
</html>